const app = new Vue({
      el:'#app',
      data() {
        return {
          step:1,
          price: '10',
          numbers: '1',
          text: '',
          name: '',
          email: '',
          phone: '',
          selected: '0',
          where: ''
        }
      },
      methods:{
        // change pages
        prev() {
          this.step--;
        },
        next() {
          this.step++;
        }
      },
      computed: {
          total: function () {
            return parseFloat(this.numbers) * parseFloat(this.price)
          },
          euro: function() {
            return (this.total / 1.895).toFixed(2)
          },
          euroDelivery: function() {
            return (this.selected / 1.895).toFixed(2)
          },
          totalPrice: function () {
            return (parseFloat(this.total) + parseFloat(this.selected)).toFixed(2)
          },
          totalPriceEuro: function() {
            return (this.totalPrice / 1.895).toFixed(2)
          }
          } 
    });